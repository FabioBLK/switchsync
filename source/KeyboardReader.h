#include "switch.h"
#include <string>
#include <string.h>

class KeyboardReader
{
private:
    SwkbdConfig kbd;
    static SwkbdTextCheckResult validate_text(char *tmp_string, size_t tmp_string_size);
    void textSentCallback(const char *str, SwkbdDecidedEnterArg *arg);
    std::string typedText;

public:
    KeyboardReader();
    ~KeyboardReader();
    void createKeyboard();
    void openKeyboard();
    std::string getTypedText();
};