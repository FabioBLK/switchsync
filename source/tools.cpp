#include "tools.h"
#include "Files.h"
#include <jansson.h>

namespace tools
{
int Clamp(int value, int min, int max)
{
    int result = value;

    if (value >= max)
    {
        result = max;
    }
    else if (value < min)
    {
        result = min;
    }

    return result;
}

void LogToFile(std::string p_data)
{
    Files files = Files();
    files.appendDataToFile("/switch/switchsync/log.txt", p_data);
}
std::string newConfigObject()
{
    json_t *jsonData = json_object();

    json_object_set_new(jsonData, "syncFolder", json_string("NULL"));
    json_object_set_new(jsonData, "dropboxAuth", json_string("NULL"));
    json_object_set_new(jsonData, "authorized", json_boolean(false));
    json_object_set_new(jsonData, "accessToken", json_string("NULL"));
    json_object_set_new(jsonData, "appKey", json_string("NULL"));
    json_object_set_new(jsonData, "appSecret", json_string("NULL"));

    char *jsonChar = json_dumps(jsonData, JSON_ENCODE_ANY);

    std::string currentConfig = jsonChar;
    return currentConfig;
}

std::string getConfigString(ConfigDataJSON p_data)
{
    json_t *jsonData = json_object();

    json_object_set_new(jsonData, "syncFolder", json_string(p_data.syncFolder.c_str()));
    json_object_set_new(jsonData, "dropboxAuth", json_string(p_data.dropboxAuth.c_str()));
    json_object_set_new(jsonData, "authorized", json_boolean(p_data.authorized));
    json_object_set_new(jsonData, "accessToken", json_string(p_data.accessToken.c_str()));
    json_object_set_new(jsonData, "appKey", json_string(p_data.appKey.c_str()));
    json_object_set_new(jsonData, "appSecret", json_string(p_data.appSecret.c_str()));

    char *jsonChar = json_dumps(jsonData, JSON_ENCODE_ANY);

    std::string currentConfig = jsonChar;
    return currentConfig;
}

ConfigDataJSON parseConfigData(std::string p_data)
{
    ConfigDataJSON result;
    json_t *jsonData;
    json_error_t jsonError;
    json_t *syncFolder;
    json_t *dropboxAuth;
    json_t *authorized;
    json_t *accessToken;
    json_t *appKey;
    json_t *appSecret;

    jsonData = json_loads(&p_data[0u], 0, &jsonError);
    if (jsonData || json_is_object(jsonData))
    {
        syncFolder = json_object_get(jsonData, "syncFolder");
        dropboxAuth = json_object_get(jsonData, "dropboxAuth");
        authorized = json_object_get(jsonData, "authorized");
        accessToken = json_object_get(jsonData, "accessToken");
        appKey = json_object_get(jsonData, "appKey");
        appSecret = json_object_get(jsonData, "appSecret");

        result.syncFolder = json_string_value(syncFolder);
        result.dropboxAuth = json_string_value(dropboxAuth);
        result.authorized = json_boolean_value(authorized);
        result.accessToken = json_string_value(accessToken);
        result.appKey = json_string_value(appKey);
        result.appSecret = json_string_value(appSecret);
    }
    return result;
}

AuthorizationTokenJSON parseAuthorizationTokenData(std::string p_data)
{
    AuthorizationTokenJSON result;
    json_t *jsonData;
    json_error_t jsonError;
    json_t *access_token;
    json_t *token_type;
    json_t *uid;
    json_t *account_id;

    jsonData = json_loads(&p_data[0u], 0, &jsonError);
    if (jsonData || json_is_object(jsonData))
    {
        access_token = json_object_get(jsonData, "access_token");
        token_type = json_object_get(jsonData, "token_type");
        uid = json_object_get(jsonData, "uid");
        account_id = json_object_get(jsonData, "account_id");

        result.access_token = json_string_value(access_token);
        result.token_type = json_string_value(token_type);
        result.uid = json_boolean_value(uid);
        result.account_id = json_boolean_value(account_id);
    }
    return result;
}

std::queue<FileInfoJSON> parseFileInfoData(std::string p_data)
{
    std::queue<FileInfoJSON> result;
    json_t *jsonRoot;
    json_error_t jsonError;

    jsonRoot = json_loads(&p_data[0u], 0, &jsonError);

    if (jsonRoot || json_is_object(jsonRoot))
    {
        json_t *jsonData;

        jsonData = json_object_get(jsonRoot, "entries");

        if (jsonData && json_is_array(jsonData))
        {
            for (int i = 0; i < json_array_size(jsonData); i++)
            {
                json_t *tag;
                json_t *name;
                json_t *path_lower;
                json_t *path_display;
                json_t *id;
                json_t *client_modified;
                json_t *server_modified;
                json_t *rev;
                json_t *size;
                json_t *is_downloadable;
                json_t *content_hash;

                json_t *indexData;

                FileInfoJSON info;

                indexData = json_array_get(jsonData, i);

                tag = json_object_get(indexData, ".tag");
                name = json_object_get(indexData, "name");
                path_lower = json_object_get(indexData, "path_lower");
                path_display = json_object_get(indexData, "path_display");
                id = json_object_get(indexData, "id");
                client_modified = json_object_get(indexData, "client_modified");
                server_modified = json_object_get(indexData, "server_modified");
                rev = json_object_get(indexData, "rev");
                size = json_object_get(indexData, "size");
                is_downloadable = json_object_get(indexData, "is_downloadable");
                content_hash = json_object_get(indexData, "content_hash");

                if (tag)
                    info.tag = json_string_value(tag);
                if (name)
                    info.name = json_string_value(name);
                if (path_lower)
                    info.path_lower = json_string_value(path_lower);
                if (path_display)
                    info.path_display = json_string_value(path_display);
                if (id)
                    info.id = json_string_value(id);
                if (client_modified)
                    info.client_modified = json_string_value(client_modified);
                if (server_modified)
                    info.server_modified = json_string_value(server_modified);
                if (rev)
                    info.rev = json_string_value(rev);
                if (size)
                    info.size = json_integer_value(size);
                if (is_downloadable)
                    info.is_downloadable = json_boolean_value(is_downloadable);
                if (content_hash)
                    info.content_hash = json_string_value(content_hash);

                result.push(info);
            }
        }
    }

    return result;
}
} // namespace tools