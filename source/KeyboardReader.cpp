#include "KeyboardReader.h"
#include "tools.h"
#include <string.h>

KeyboardReader::KeyboardReader()
{
}

KeyboardReader::~KeyboardReader()
{
}

SwkbdTextCheckResult KeyboardReader::validate_text(char *tmp_string, size_t tmp_string_size)
{
    if (strcmp(tmp_string, "bad") == 0)
    {
        strncpy(tmp_string, "Bad string.", tmp_string_size);
        return SwkbdTextCheckResult_Bad;
    }

    return SwkbdTextCheckResult_OK;
}

void KeyboardReader::textSentCallback(const char *str, SwkbdDecidedEnterArg *arg)
{
    typedText = str;
}

void KeyboardReader::createKeyboard()
{
    Result rc;

    rc = swkbdCreate(&kbd, 0);

    if (R_SUCCEEDED(rc))
    {
        swkbdConfigMakePresetDefault(&kbd);
        swkbdConfigSetTextCheckCallback(&kbd, validate_text);
    }
}

std::string KeyboardReader::getTypedText()
{
    if (typedText.empty())
    {
        return "";
    }
    return typedText;
}

void KeyboardReader::openKeyboard()
{
    static char tmpoutstr[256];
    Result rc2 = swkbdShow(&kbd, tmpoutstr, sizeof(tmpoutstr));

    if (R_SUCCEEDED(rc2))
    {
        typedText = tmpoutstr;
    }

    swkbdClose(&kbd);
}