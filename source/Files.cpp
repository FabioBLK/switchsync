#include "Files.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <queue>

Files::Files()
{
}

Files::~Files()
{
}

bool Files::getDirData(std::string p_path, std::vector<FilesData> *p_data)
{
    struct dirent *entry;
    struct stat fileStat;
    DIR *dir = opendir(&p_path[0u]);
    if (dir == NULL)
    {
        return false;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        FilesData data;
        data.isFolder = (entry->d_type == DT_DIR) ? true : false;
        data.name = entry->d_name;
        if (!stat(entry->d_name, &fileStat))
        {
            data.fileSize = (int)fileStat.st_size;
            data.mtime = fileStat.st_mtime;
        }
        else
        {
            data.fileSize = 0;
        }
        data.selectedToTransfer = false;
        p_data->push_back(data);
    }

    closedir(dir);
    return true;
}

bool Files::getDirDataQueue(std::string p_path, std::queue<FilesData> *p_data)
{
    struct dirent *entry;
    struct stat fileStat;
    DIR *dir = opendir(&p_path[0u]);
    if (dir == NULL)
    {
        return false;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        FilesData data;
        data.isFolder = (entry->d_type == DT_DIR) ? true : false;
        data.name = entry->d_name;
        if (!stat(entry->d_name, &fileStat))
        {
            data.fileSize = (int)fileStat.st_size;
            data.mtime = fileStat.st_mtime;
        }
        else
        {
            data.fileSize = 0;
        }
        data.selectedToTransfer = false;
        p_data->push(data);
    }

    closedir(dir);
    return true;
}

bool Files::isDirValid(std::string p_path)
{
    DIR *dir = opendir(&p_path[0u]);
    if (dir == NULL)
    {
        return false;
    }
    return false;
}

bool Files::createDir(std::string p_path)
{
    mkdir(&p_path[0u], 0777);
    return isDirValid(p_path);
}

int Files::getFileSize(std::string p_path)
{
    std::streampos begin, end;
    std::ifstream fileStream;
    fileStream.open(p_path);

    if (fileStream.is_open())
    {
        begin = fileStream.tellg();
        fileStream.seekg(0, std::ios::end);
        end = fileStream.tellg();
        fileStream.close();

        return end - begin;
    }
    return 0;
}

bool Files::loadFileTXTData(std::string p_path, std::string *p_data)
{
    std::ifstream fileStream;
    fileStream.open(p_path);

    std::string line;
    if (fileStream.is_open())
    {
        fileStream.seekg(0, std::ios::end);
        p_data->reserve(fileStream.tellg());
        fileStream.seekg(0, std::ios::beg);

        p_data->assign((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());

        return true;
    }

    return false;
}

bool Files::loadFileData(std::string p_path, FILE *p_fp, char *p_data, int p_fileSize)
{
    p_fp = fopen(&p_path[0u], "rb");

    if (!p_fp)
    {
        return false;
    }

    if ((size_t)p_fileSize != fread(p_data, 1, (size_t)p_fileSize, p_fp))
    {
        return false;
    }

    return true;
}

bool Files::saveFileData(std::string p_path, std::string p_data)
{
    std::ofstream fileStream;
    fileStream.open(p_path, std::ios::trunc);

    std::string line;
    if (fileStream.is_open())
    {
        fileStream << p_data;
        fileStream.close();

        return true;
    }

    return false;
}

std::ofstream *Files::openFileToSave(std::string p_path)
{
    std::ofstream output(p_path, std::ofstream::out | std::ofstream::app);
    return &output;
}

FILE *Files::openFileToDownload(std::string p_path)
{
    FILE *fp = fopen(&p_path[0u], "wb");
    return fp;
}

void Files::closeFile(std::ofstream *p_id)
{
    p_id->close();
}

bool Files::appendDataToFile(std::string p_path, std::string p_data)
{
    std::ofstream fileStream(p_path, std::fstream::in | std::fstream::out | std::fstream::app);

    if (fileStream.is_open())
    {
        fileStream.write(&p_data[0u], p_data.size());
        fileStream.close();

        return true;
    }

    return false;
}

size_t Files::writeCurlDataCallBack(char *ptr, size_t size, size_t nmemb, void *userdata)
{
    std::ofstream *fout = (std::ofstream *)userdata;
    for (size_t x = 0; x < nmemb; x++)
    {
        *fout << ptr[x];
    }

    return size * nmemb;
}

size_t Files::readCurlDataCallBack(char *buffer, size_t size, size_t nitems, void *instream)
{
    size_t bytes_read;

    bytes_read = fread(buffer, 1, (size * nitems), (FILE *)instream);

    return bytes_read;
}