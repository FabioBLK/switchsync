#include "UIManager.h"
#include "switch.h"
#include "tools.h"
#include <SDL2/SDL_image.h>

UIManager::UIManager()
{
}

UIManager::~UIManager()
{
}

int UIManager::StartUI()
{
    Result fsResult = romfsInit();
    if (R_FAILED(fsResult))
    {
        printf("Error loading romfs");
        return -1;
    }

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        SDL_Log("SDL_Init: %s\n", SDL_GetError());
        return -1;
    }

    window = SDL_CreateWindow("SwitchSync", 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGTH, 0);
    if (!window)
    {
        SDL_Log("SDL_CreateWindow: %s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }

    renderer = SDL_CreateRenderer(window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!renderer)
    {
        SDL_Log("SDL_CreateRenderer: %s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }

    defaultFont = FC_CreateFont();
    FC_LoadFont(defaultFont, renderer, "romfs:/res/font/droidsans.ttf", 20, FC_MakeColor(0, 0, 0, 255), TTF_STYLE_NORMAL);

    return 1;
}

void UIManager::BeginFrame()
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    // fill window bounds
    SDL_SetRenderDrawColor(renderer, 192, 192, 192, 255);
    SDL_GetWindowSize(window, &DISPLAY_WIDTH, &DISPLAY_HEIGTH);
    SDL_Rect f = {0, 0, DISPLAY_WIDTH, DISPLAY_HEIGTH};
    SDL_RenderFillRect(renderer, &f);
}

SDL_Texture *UIManager::LoadTexture(std::string p_path)
{
    IMG_Init(IMG_INIT_PNG);
    return IMG_LoadTexture(renderer, &p_path[0u]);
}

void UIManager::DrawTexture(SDL_Texture *p_textureRef, int p_xPos, int p_yPos, int p_width, int p_heigth)
{
    SDL_Rect texr;
    texr.x = p_xPos;
    texr.y = p_yPos;
    texr.w = p_width;
    texr.h = p_heigth;
    SDL_RenderCopy(renderer, p_textureRef, NULL, &texr);
}

void UIManager::UnloadTexture(SDL_Texture *p_textureRef)
{
    SDL_DestroyTexture(p_textureRef);
}

void UIManager::DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const SDL_Color p_color, const int p_size)
{
    FC_DrawColor(defaultFont, renderer, p_xPosition, p_yPosition, p_color, &p_text[0u]);
}

void UIManager::DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, SDL_Color p_color)
{
    SDL_SetRenderDrawColor(renderer, p_color.r, p_color.g, p_color.b, p_color.a);
    SDL_Rect rect;
    rect.x = p_xPos;
    rect.y = p_yPos;
    rect.w = p_width;
    rect.h = p_height;
    SDL_RenderFillRect(renderer, &rect);
}

void UIManager::DrawDefaultBGRectangle(SDL_Color p_color)
{
    int startXPosition = START_BOX_Y_POSITION;
    int endXPosition = START_BOX_Y_POSITION * 2;
    int maxY = DISPLAY_HEIGTH - END_BOX_Y_POSITION;

    SDL_SetRenderDrawColor(renderer, p_color.r, p_color.g, p_color.b, p_color.a);
    SDL_Rect rect;
    rect.x = startXPosition;
    rect.y = START_BOX_Y_POSITION;
    rect.w = DISPLAY_WIDTH - endXPosition;
    rect.h = maxY;
    SDL_RenderFillRect(renderer, &rect);
}

void UIManager::DrawDirList(const std::vector<FilesData> &p_list, int p_selected, bool p_fileSelectionEnabled, SDL_Color p_bgColor)
{
    int selected = p_selected;
    int startXPosition = START_BOX_Y_POSITION;
    int endXPosition = START_BOX_Y_POSITION * 2;
    int maxY = DISPLAY_HEIGTH - END_BOX_Y_POSITION;

    DrawDefaultBGRectangle(p_bgColor);

    int maxBoxes = ((maxY - START_BOX_Y_POSITION) / DIR_BOX_SIZE) + 1;
    maxBoxes = tools::Clamp(maxBoxes, 0, p_list.size());
    if (selected >= boxStartIndex + maxBoxes)
    {
        boxStartIndex++;
    }
    else if (selected < boxStartIndex)
    {
        boxStartIndex--;
    }

    boxStartIndex = tools::Clamp(boxStartIndex, 0, p_list.size());
    selected = (selected - boxStartIndex);

    for (int i = 0; i < maxBoxes; i++)
    {
        SDL_Color color = selected == i ? selectedTextColor : regularTextColor;
        std::string text = p_list[boxStartIndex + i].name;
        int position = (i * DIR_BOX_SIZE) + START_BOX_Y_POSITION;
        DrawDirBox(position, text, p_list[boxStartIndex + i].isFolder, color,
                   (p_fileSelectionEnabled && !p_list[boxStartIndex + i].isFolder), p_list[boxStartIndex + i].selectedToTransfer);
    }
    //DrawText(std::to_string(p_selected) + " | startIndex = " + std::to_string(boxStartIndex));
}

void UIManager::DrawDirBox(const int p_startPosY, const std::string &p_text, const bool p_isFolder,
                           const SDL_Color &p_textColor, const bool p_fileSelectionEnabled,
                           const bool p_showSelectionMark)
{
    int textPos = p_startPosY + 60;
    DrawText(p_text, 200, textPos, p_textColor, 20);
    std::string fileType = (p_isFolder) ? "FOLDER" : "FILE";
    DrawText(fileType, DISPLAY_WIDTH - 500, textPos, p_textColor, 20);

    if (p_fileSelectionEnabled)
    {
        SDL_Color fileSelectedColor = (p_showSelectionMark) ? SDL_Color{0, 150, 0, 150} : SDL_Color{0, 0, 0, 255};
        DrawBGRectangle(120, textPos, 35, 35, fileSelectedColor);
    }

    SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
    SDL_Rect rect;
    rect.x = 100;
    rect.y = p_startPosY;
    rect.w = DISPLAY_WIDTH - 200;
    rect.h = DIR_BOX_SIZE + 1;
    SDL_RenderDrawRect(renderer, &rect);
}

void UIManager::DrawOptionBox(const int p_startPosY, const std::string &p_text, const SDL_Color &p_textColor)
{
    int textPos = p_startPosY + 60;
    DrawText(p_text, 200, textPos, p_textColor, 20);

    SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
    
    SDL_Rect rect;
    rect.x = 100;
    rect.y = p_startPosY;
    rect.w = DISPLAY_WIDTH - 200;
    rect.h = DIR_BOX_SIZE + 1;
    SDL_RenderDrawRect(renderer, &rect);
    
}

void UIManager::DrawSyncInstructionsView(std::string &p_syncUrl, SDL_Texture *p_qrTexture, SDL_Color &p_bgColor, SDL_Color &p_textColor)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("DROPBOX Account Sync", 70, 25, p_textColor, 20);
    DrawText("To link this app with your DROPBOX account, please open the url below and authorize the app: ", 70, 75, p_textColor, 20);
    DrawText(p_syncUrl, 70, 105, p_textColor, 30);
    DrawText("or scan the QRCode below using your phone", 70, 135, p_textColor, 20);
    DrawTexture(p_qrTexture, (DISPLAY_WIDTH / 2) - 250, 200, 500, 500);
    DrawText("After authorizing the app in the browser, you'll receive a code.", 70, 750, p_textColor, 20);
    DrawText("Press \"+\" when you're ready to type the code", 70, 770, p_textColor, 20);
    DrawText("Press \"+\" to type the code from Dropbox", 70, DISPLAY_HEIGTH - 60, p_textColor, 14);
}

void UIManager::DrawAuthorizingView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_authCode, std::string &p_authResult, std::string &p_downloadedStatus)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("Authorizing Dropbox Account", 70, 25, p_textColor, 20);
    DrawText("Performing Dropbox authorization using key: ", 70, 75, p_textColor, 20);
    DrawText("Press \"B\" to type another authorization code", 70, 530, p_textColor, 14);
    DrawText(p_authCode, 70, 95, p_textColor, 20);
    DrawText(p_authResult, 70, 400, p_textColor, 14);
    DrawText(p_downloadedStatus, 70, 510, p_textColor, 14);
}

void UIManager::DrawSelectTransferView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadStatus, std::string &p_currentLocalFolder)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer", 70, 25, p_textColor, 20);
    DrawText("Press \"A\" button to start download", 70, 75, p_textColor, 20);
    DrawText("Press \"B\" button to start upload", 70, 95, p_textColor, 20);
    DrawText(p_downloadStatus, 70, 120, p_textColor, 20);
    //DrawText("Press \"-\" to choose another folder", 70, DISPLAY_HEIGTH - 60, p_textColor, 14);
    DrawText("Current Selected Folder - " + p_currentLocalFolder, 70, DISPLAY_HEIGTH - 40, p_textColor, 14);
}

void UIManager::DrawFinishTransferView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadStatus, std::string &p_errorReport, std::string &p_currentLocalFolder)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer Finished", 70, 25, p_textColor, 20);
    DrawText("Press A button to start Start Over", 70, 75, p_textColor, 20);
    DrawText(p_downloadStatus, 70, 120, p_textColor, 20);
    if (!p_errorReport.empty())
    {
        DrawText("Error Log: " + p_errorReport, 70, 150, p_textColor, 20);
    }

    DrawText("Current Selected Folder - " + p_currentLocalFolder, 70, DISPLAY_HEIGTH - 40, p_textColor, 14);
}

void UIManager::DrawTransferProgressView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadType, std::string &p_downloadStatus, std::string p_downloadCountStatus)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("File Transfer", 70, 25, p_textColor, 20);
    DrawText(p_downloadType + " IN PROGRESS", 70, 75, p_textColor, 20);
    DrawText(p_downloadStatus, 70, 120, p_textColor, 20);
    DrawText(p_downloadCountStatus, 70, 140, p_textColor, 20);
}

void UIManager::DrawConfigScreenView(std::string p_url, SDL_Color &p_bgColor, SDL_Color &p_textColor, SDL_Texture *p_textureRef,
                                     std::vector<std::string> &p_optionsList, int p_selectPosition, int p_startBoxYPosition)
{
    DrawDefaultBGRectangle(p_bgColor);
    DrawText("Initial Settings", 70, 25, p_textColor, 20);
    DrawText("Welcome to SWITCHSYNC.", 100, 100, p_textColor, 20);
    DrawText("To start using this app you need to have a Dropbox App account created.", 100, 140, p_textColor, 20);
    DrawText("Once your Dropbox account is configured, fill in the AppKey and AppSecret.", 100, 160, p_textColor, 20);
    DrawText("For instructions on how to setup a Dropbox App Acccount, Please check the URL below, or open the QRCode with your smartphone", 100, 200, p_textColor, 20);
    DrawText(p_url, 100, 220, p_textColor, 20);
    DrawTexture(p_textureRef, (DISPLAY_WIDTH / 2) - 200, 250, 400, 400);

    DrawText("Use the directional keys to change selection and press \"A\" to select.", 70, DISPLAY_HEIGTH - 60, p_textColor, 14);
    DrawText("Press \"+\" to save the settings and continue.", 70, DISPLAY_HEIGTH - 40, p_textColor, 14);

    for (int i = 0; i < p_optionsList.size(); i++)
    {
        SDL_Color color = p_selectPosition == i ? selectedTextColor : regularTextColor;
        std::string text = p_optionsList[i];
        int boxPosition = (i * DIR_BOX_SIZE) + p_startBoxYPosition;
        DrawOptionBox(boxPosition, text, color);
    }
}

void UIManager::EndFrame()
{
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    SDL_RenderPresent(renderer);
}

void UIManager::FinishUI()
{
    FC_FreeFont(defaultFont);
    romfsExit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}