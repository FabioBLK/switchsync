#include "FontCache/SDL_FontCache.h"
#include "Files.h"

#include <SDL2/SDL.h>
#include <string>
#include <vector>

static int DISPLAY_WIDTH = 1920;
static int DISPLAY_HEIGTH = 1080;
const static int MAX_FONT_SIZE = 42;
const static int START_BOX_Y_POSITION = 60;
const static int END_BOX_Y_POSITION = 120;
const static int DIR_BOX_SIZE = 120;

class UIManager
{
private:
    SDL_Window *window;
    SDL_Renderer *renderer;
    int boxStartIndex = 0;
    FC_Font *defaultFont;
    const SDL_Color selectedTextColor = {255, 0, 0, 255};
    const SDL_Color regularTextColor = {0, 0, 0, 255};

public:
    UIManager();
    ~UIManager();
    int StartUI();
    void FinishUI();
    void BeginFrame();
    void EndFrame();
    void DrawText(const std::string &p_text, const int p_xPosition, const int p_yPosition, const SDL_Color p_color, const int p_size);
    void DrawDirList(const std::vector<FilesData> &p_list, int p_selected, bool p_fileSelectionEnabled, SDL_Color p_bgColor);
    void DrawDirBox(const int p_startPosY, const std::string &p_text, const bool p_isFolder, const SDL_Color &p_textColor, const bool p_selectionMark, const bool p_showSelectionMark);
    void DrawOptionBox(const int p_startPosY, const std::string &p_text, const SDL_Color &p_textColor);
    SDL_Texture *LoadTexture(std::string p_path);
    void DrawTexture(SDL_Texture *p_textureRef, int p_xPos, int p_yPos, int p_width, int p_heigth);
    void UnloadTexture(SDL_Texture *p_textureRef);
    void DrawBGRectangle(int p_xPos, int p_yPos, int p_width, int p_height, SDL_Color p_color);
    void DrawDefaultBGRectangle(SDL_Color p_color);
    void DrawSyncInstructionsView(std::string &p_syncUrl, SDL_Texture *p_qrTexture, SDL_Color &p_bgColor, SDL_Color &p_textColor);
    void DrawAuthorizingView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_authCode, std::string &p_authResult, std::string &p_downloadedStatus);
    void DrawFinishTransferView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadStatus, std::string &p_errorReport, std::string &p_currentLocalFolder);
    void DrawSelectTransferView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadStatus, std::string &p_currentLocalFolder);
    void DrawTransferProgressView(SDL_Color &p_bgColor, SDL_Color &p_textColor, std::string &p_downloadType, std::string &p_downloadStatus, std::string p_downloadCountStatus);
    void DrawConfigScreenView(std::string p_url, SDL_Color &p_bgColor, SDL_Color &p_textColor, SDL_Texture *p_textureRef,
                                     std::vector<std::string> &p_optionsList, int p_selectPosition, int p_startBoxYPosition);
};