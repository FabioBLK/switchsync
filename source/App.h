#include "UIManager.h"
#include "tools.h"

const int BUTTON_SLEEP = 100000;
const int SLEEP_TIME = 10;

enum AppStatus
{
    Directory,
    RemoteDirectory,
    SyncInstructions,
    Authorizing,
    FileTransferDownload,
    FileTransferUpload,
    CheckStatus,
    SelectTransferAction,
    SelectLocalFiles,
    SelectRemoteFiles,
    ConfigScreen,
    Exit
};

class App
{
private:
    int buttonTime = 0;
    std::string startPath = "/";
    std::string rootPath = "/";
    std::string configFilePath = "/switch/switchsync/config.json";
    std::string remoteFolder;
    SDL_Color colorBlack = {.r = 0, .g = 0, .b = 0, .a = 255};
    SDL_Color bgColorWhiteTransparent = {.r = 255, .g = 255, .b = 255, .a = 230};
    UIManager uiManager;
    AppStatus status = AppStatus::Directory;
    std::queue<FilesData> selectedLocalFiles;
    ConfigDataJSON configData;
    void SetStatus();
    void DirectoryView();
    void SyncInstructions();
    void FileTransferView();
    void AuthorizingView();
    void SelectTransferActionView();
    void ConfigScreenView();
    ConfigDataJSON CheckConfig();

public:
    App();
    ~App();
    void StartApp();
};
