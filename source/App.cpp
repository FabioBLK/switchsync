#include "App.h"
#include "qrCode/QrCode.hpp"
#include "KeyboardReader.h"
#include "Downloader.h"
#include <vector>
#include <switch.h>

App::App()
{
}

App::~App()
{
}

ConfigDataJSON App::CheckConfig()
{
    Files files = Files();
    std::string configDir = "/switch/switchsync";
    bool exists = files.isDirValid(configDir);
    if (!exists)
    {
        bool dirCreated = files.createDir(configDir);
    }

    std::string currentConfig = "";
    files.loadFileTXTData(configFilePath, &currentConfig);

    if (currentConfig.size() == 0)
    {
        currentConfig = tools::newConfigObject();
        files.saveFileData(configFilePath, currentConfig);
    }

    return tools::parseConfigData(currentConfig);
}

void App::SetStatus()
{
    if (configData.appKey == "NULL" || configData.appSecret == "NULL")
    {
        status = AppStatus::ConfigScreen;
    }
    else if (configData.dropboxAuth == "NULL" || configData.dropboxAuth.empty())
    {
        status = AppStatus::SyncInstructions;
    }
    else if (!configData.authorized)
    {
        status = AppStatus::Authorizing;
    }
    else
    {
        status = AppStatus::SelectTransferAction;
    }
}

void App::StartApp()
{
    configData = CheckConfig();
    int uiStart = uiManager.StartUI();

    if (uiStart < 0)
    {
        printf("Error starting ui");
        return;
    }

    SetStatus();

    while (status != AppStatus::Exit)
    {
        if (status == AppStatus::Directory || status == AppStatus::RemoteDirectory)
        {
            DirectoryView();
        }
        else if (status == AppStatus::ConfigScreen)
        {
            ConfigScreenView();
        }
        else if (status == AppStatus::SyncInstructions)
        {
            SyncInstructions();
        }
        else if (status == AppStatus::Authorizing)
        {
            AuthorizingView();
        }
        else if (status == AppStatus::FileTransferDownload || status == AppStatus::FileTransferUpload)
        {
            FileTransferView();
        }
        else if (status == AppStatus::SelectTransferAction)
        {
            SelectTransferActionView();
        }
        else if (status == AppStatus::SelectLocalFiles || status == AppStatus::SelectRemoteFiles)
        {
            DirectoryView();
        }
        else if (status == AppStatus::CheckStatus)
        {
            SetStatus();
        }
        else
        {
            status = AppStatus::Exit;
        }
    }

    uiManager.FinishUI();
}

void App::DirectoryView()
{
    int position = 0;

    Files files = Files();
    std::vector<FilesData> filesData;
    Downloader downloader = Downloader();
    bool downloadingRemoteList = false;
    bool localFilesView = (status == AppStatus::SelectLocalFiles || status == AppStatus::Directory);
    bool selectionEnabled = (status == AppStatus::SelectLocalFiles || status == AppStatus::SelectRemoteFiles);
    bool allSelected = false;
    std::string downloadStatus;
    std::queue<FileInfoJSON> fileInfoList;
    std::string pathString = (configData.syncFolder.empty() || configData.syncFolder == "NULL") ? startPath : configData.syncFolder;
    std::string alert = "";
    std::string viewInstructionsText = "Select the local files you want to upload to your Dropbox folder";

    SDL_Texture *bgImage;

    if (localFilesView)
    {
        files.getDirData(pathString, &filesData);
        bgImage = uiManager.LoadTexture("romfs:/res/sdcard.png");
        if (selectionEnabled)
        {
            viewInstructionsText = "Select the local to download the previously selected files";
        }
    }
    else
    {
        pathString = "";
        downloadingRemoteList = true;
        bgImage = uiManager.LoadTexture("romfs:/res/cloud.png");

        if (selectionEnabled)
        {
            viewInstructionsText = "Select the remote files you want to download to your SDCard";
        }
        else
        {
            viewInstructionsText = "Select the remote folder you want to upload the previously selected files";
        }
    }

    while (appletMainLoop())
    {
        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (downloadingRemoteList)
        {
            std::string remoteListoutput;
            uiManager.BeginFrame();

            uiManager.DrawTexture(bgImage, DISPLAY_WIDTH / 2 - 256, DISPLAY_HEIGTH / 2 - 256, 500, 500);
            uiManager.DrawText(downloadStatus, 70, 25, colorBlack, 20);
            uiManager.DrawText("Downloading Remote file list. Please wait.", DISPLAY_WIDTH - 550, 25, colorBlack, 14);

            uiManager.EndFrame();
            
            pathString = (pathString == "/") ? "" : pathString;
            int response = downloader.DropboxRequestFileList(configData.accessToken, pathString, &remoteListoutput);
            if (response != 200)
            {
                downloadStatus = "Error downloading file list from server. Error code: " + std::to_string(response);
            }
            else
            {
                fileInfoList = tools::parseFileInfoData(remoteListoutput);
                while (!fileInfoList.empty())
                {
                    FilesData newFileData;
                    newFileData.name = fileInfoList.front().name;
                    newFileData.fileSize = fileInfoList.front().size;
                    newFileData.isFolder = fileInfoList.front().tag == "folder";
                    newFileData.selectedToTransfer = false;

                    filesData.push_back(newFileData);
                    fileInfoList.pop();
                }
            }
            downloadingRemoteList = false;
        }

        if ((kDown & KEY_LSTICK_DOWN) || (kDown & KEY_DDOWN) || (kDown & KEY_L))
        {
            position++;
            buttonTime = 0;
        }
        else if ((kDown & KEY_LSTICK_UP) || (kDown & KEY_DUP) || (kDown & KEY_R))
        {
            position--;
            buttonTime = 0;
        }
        else if (kDown & KEY_A)
        {
            alert = "";
            if (position <= filesData.size() - 1)
            {
                if (filesData[position].isFolder)
                {
                    pathString += filesData[position].name + "/";

                    filesData.clear();
                    if (localFilesView)
                    {
                        files.getDirData(pathString, &filesData);
                    }
                    else
                    {
                        if (pathString.find_first_of('/', pathString.size()) > 0)
                        {
                            pathString = "/" + pathString;
                        }
                        downloadingRemoteList = true;
                    }

                    position = 0;
                }
            }

            buttonTime = 0;
        }
        else if (kDown & KEY_B)
        {
            std::string upperFolder = pathString;
            if (upperFolder.length() > 0)
            {
                upperFolder.pop_back();
            }

            int foundPos = upperFolder.find_last_of("/");

            if (foundPos >= 0)
            {
                pathString = upperFolder.substr(0, foundPos) + "/";
                filesData.clear();
                if (localFilesView)
                {
                    files.getDirData(pathString, &filesData);
                }
                else
                {
                    downloadingRemoteList = true;
                }
                position = 0;
            }

            buttonTime = 0;
        }
        else if (kDown & KEY_X)
        {
            if (!filesData[position].isFolder)
            {
                bool markedToTransfer = !filesData[position].selectedToTransfer;
                filesData[position].selectedToTransfer = markedToTransfer;
            }
            buttonTime = 0;
        }
        else if (kDown & KEY_Y)
        {
            allSelected = !allSelected;
            for (int i = 0; i < filesData.size(); i++)
            {
                if (!filesData[i].isFolder)
                {
                    filesData[i].selectedToTransfer = allSelected;
                }
            }
            buttonTime = 0;
        }
        else if (kDown & KEY_PLUS)
        {
            if (status == AppStatus::Directory || status == AppStatus::SelectLocalFiles)
            {
                configData.syncFolder = pathString;

                std::string configFile = tools::getConfigString(configData);
                files.saveFileData(configFilePath, configFile);
            }

            if (status == AppStatus::SelectLocalFiles)
            {
                while (!selectedLocalFiles.empty())
                {
                    selectedLocalFiles.pop();
                }

                for (int i = 0; i < filesData.size(); i++)
                {
                    if (filesData[i].selectedToTransfer)
                    {
                        selectedLocalFiles.push(filesData[i]);
                    }
                }

                status = AppStatus::RemoteDirectory;
                break;
            }
            else if (status == AppStatus::SelectRemoteFiles)
            {
                while (!selectedLocalFiles.empty())
                {
                    selectedLocalFiles.pop();
                }

                for (int i = 0; i < filesData.size(); i++)
                {
                    if (filesData[i].selectedToTransfer)
                    {
                        selectedLocalFiles.push(filesData[i]);
                    }
                }

                remoteFolder = pathString;
                status = AppStatus::Directory;
                break;
            }
            else if (status == AppStatus::Directory)
            {
                if (pathString != rootPath)
                {
                    status = AppStatus::FileTransferDownload;
                    break;
                }
                else
                {
                    alert = "You cant select a root folder to Download";
                }
            }
            else if (status == AppStatus::RemoteDirectory)
            {
                remoteFolder = pathString;
                status = AppStatus::FileTransferUpload;
                break;
            }
            else
            {
                status == AppStatus::SelectTransferAction;
                break;
            }
        }

        if (kDown & KEY_MINUS)
        {
            status = AppStatus::Exit;
            break;
        }

        uiManager.BeginFrame();

        uiManager.DrawTexture(bgImage, DISPLAY_WIDTH / 2 - 256, DISPLAY_HEIGTH / 2 - 256, 500, 500);

        int listPosition = tools::Clamp(position, 0, filesData.size() - 1);
        position = listPosition;
        uiManager.DrawDirList(filesData, listPosition, selectionEnabled, bgColorWhiteTransparent);
        uiManager.DrawText(alert, DISPLAY_WIDTH - 250, DISPLAY_HEIGTH - 50, colorBlack, 14);
        uiManager.DrawText("", DISPLAY_WIDTH - 550, 25, colorBlack, 14);
        uiManager.DrawText("Current Path: " + pathString, 70, 35, colorBlack, 20);
        uiManager.DrawText(viewInstructionsText, 70, 15, colorBlack, 20);
        uiManager.DrawText("\"A\" - Enter folder     \"B\" - Exit folder     \"Y\" - Select single file    \"X\" - Select all files     \"PLUS\" - Confirm Selection and advance", 70, DISPLAY_HEIGTH - 50, colorBlack, 20);
        uiManager.DrawText(downloadStatus, DISPLAY_WIDTH - 550, DISPLAY_HEIGTH - 50, colorBlack, 20);
        uiManager.EndFrame();
    }
}

void App::SyncInstructions()
{
    Files files = Files();
    std::string url = "https://www.dropbox.com/oauth2/authorize?client_id=" + configData.appKey + "&response_type=code";
    qrcodegen::QrCode qr0 = qrcodegen::QrCode::encodeText(&url[0u], qrcodegen::QrCode::Ecc::MEDIUM);
    std::string svg = qr0.toSvgString(4);

    std::string svgPath = "/switch/switchsync/svgfile.svg";
    files.saveFileData(svgPath, svg);
    SDL_Texture *image = uiManager.LoadTexture(&svgPath[0u]);

    KeyboardReader kb = KeyboardReader();

    std::string userInput = "NULL";
    char *cArr = new char[512];

    while (appletMainLoop())
    {
        uiManager.BeginFrame();

        uiManager.DrawSyncInstructionsView(url, image, bgColorWhiteTransparent, colorBlack);

        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (kDown & KEY_PLUS)
        {
            kb.createKeyboard();
            kb.openKeyboard();
            userInput = kb.getTypedText();
        }
        if (kDown & KEY_MINUS)
        {
            status = AppStatus::Exit;
            break;
        }

        if (!userInput.empty() && userInput != "NULL")
        {
            configData.dropboxAuth = userInput;

            std::string configFile = tools::getConfigString(configData);
            files.saveFileData(configFilePath, configFile);
            status = AppStatus::Authorizing;
            delete cArr;
            uiManager.UnloadTexture(image);
            break;
        }

        uiManager.EndFrame();
    }
}

void App::AuthorizingView()
{
    bool authorizing = false;
    Files files = Files();

    Downloader downloader = Downloader();
    std::string authResult = "";
    std::string downloadedStatus = "Waiting";

    while (appletMainLoop())
    {
        uiManager.BeginFrame();

        uiManager.DrawAuthorizingView(bgColorWhiteTransparent, colorBlack, configData.dropboxAuth, authResult, downloadedStatus);

        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (kDown & KEY_PLUS)
        {
            authorizing = false;
        }

        if (kDown & KEY_B)
        {
            configData.dropboxAuth = "NULL";

            std::string configFile = tools::getConfigString(configData);
            files.saveFileData(configFilePath, configFile);

            status = AppStatus::SyncInstructions;
            break;
        }

        uiManager.EndFrame();

        if (!authorizing)
        {
            authorizing = true;
            int response = downloader.DropboxRequestToken(configData.dropboxAuth, configData.appKey, configData.appSecret, &authResult);
            if (response == 200)
            {
                if (!authResult.empty())
                {
                    AuthorizationTokenJSON authData = tools::parseAuthorizationTokenData(authResult);
                    if (!authData.access_token.empty())
                    {
                        configData.accessToken = authData.access_token;
                        configData.authorized = true;
                        downloadedStatus = "Completed";

                        std::string configFile = tools::getConfigString(configData);
                        files.saveFileData(configFilePath, configFile);
                        status = AppStatus::SelectTransferAction;
                        break;
                    }
                    else
                    {
                        downloadedStatus = "Error loading the token code. Press \"+\" to try again";
                    }
                }
                else
                {
                    downloadedStatus = "Check if your typed code is correct. Press \"+\" to try again";
                }
            }
            else
            {
                downloadedStatus = "Error Connecting to the Endpoint. Press \"+\" to try again";
            }
        }
    }
}

void App::FileTransferView()
{
    std::string output;
    std::string downloadStatus;
    std::string downloadCountStatus;
    std::string errorReport;
    int successCount = 0;
    int failureCount = 0;
    Downloader downloader = Downloader();
    std::queue<FileInfoJSON> fileInfoList;
    std::queue<FilesData> *localFiles = &selectedLocalFiles;
    Files files = Files();
    bool downloading = (status == AppStatus::FileTransferDownload);
    bool uploading = (status == AppStatus::FileTransferUpload);
    int downloadCount = 0;
    std::string typeText;

    while (appletMainLoop())
    {
        if (!downloading && !uploading)
        {
            uiManager.BeginFrame();
            uiManager.DrawFinishTransferView(bgColorWhiteTransparent, colorBlack, downloadStatus, errorReport, configData.syncFolder);
            uiManager.EndFrame();
        }

        if (downloading)
        {
            uiManager.BeginFrame();
            typeText = "DOWNLOAD";
            uiManager.DrawTransferProgressView(bgColorWhiteTransparent, colorBlack, typeText, downloadStatus, downloadCountStatus);

            uiManager.EndFrame();

            if (localFiles->empty())
            {
                downloadCount = 0;
                downloadStatus = "Download Complete";
                downloading = false;
            }
            else
            {
                if (!localFiles->front().isFolder)
                {
                    downloadStatus = "Downloading File " + localFiles->front().name;
                    downloadCountStatus = std::to_string(localFiles->size()) + " files left";
                    remoteFolder = (remoteFolder.empty()) ? "/" : remoteFolder;
                    int downloadResult = downloader.DropboxDownloadFile(configData.accessToken, remoteFolder + localFiles->front().name, configData.syncFolder + localFiles->front().name);
                    if (downloadResult == 200)
                    {
                        successCount++;
                    }
                    else
                    {
                        failureCount++;
                        std::string report = "\nFailed To Download file " + localFiles->front().name + " - Error code " + std::to_string(downloadResult);
                        errorReport += report;
                    }
                }
                localFiles->pop();
            }
        }

        if (uploading)
        {
            uiManager.BeginFrame();

            typeText = "UPLOAD";
            uiManager.DrawTransferProgressView(bgColorWhiteTransparent, colorBlack, typeText, downloadStatus, downloadCountStatus);

            uiManager.EndFrame();

            if (localFiles->empty())
            {
                downloadStatus = "Upload Complete";
                uploading = false;
            }
            else
            {
                if (!localFiles->front().isFolder)
                {
                    downloadStatus = "Uploading File " + localFiles->front().name;
                    downloadCountStatus = std::to_string(localFiles->size()) + " files left";
                    remoteFolder = (remoteFolder.empty()) ? "/" : remoteFolder;
                    int downloadResult = downloader.DropboxUploadFile(configData.accessToken, remoteFolder + localFiles->front().name, configData.syncFolder + localFiles->front().name);
                    if (downloadResult == 200)
                    {
                        successCount++;
                    }
                    else
                    {
                        failureCount++;
                        std::string report = "\nFailed To Upload file " + localFiles->front().name + " - Error code " + std::to_string(downloadResult);
                        errorReport += report;
                    }
                }

                localFiles->pop();
            }
        }

        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (kDown & KEY_A)
        {
            status = AppStatus::SelectTransferAction;
            break;
        }
        if (kDown & KEY_MINUS)
        {
            status = AppStatus::Exit;
            downloader.~Downloader();
            break;
        }
    }
}

void App::SelectTransferActionView()
{
    std::string downloadStatus;

    while (appletMainLoop())
    {
        uiManager.BeginFrame();
        uiManager.DrawSelectTransferView(bgColorWhiteTransparent, colorBlack, downloadStatus, configData.syncFolder);
        uiManager.EndFrame();

        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (kDown & KEY_A)
        {
            status = AppStatus::SelectRemoteFiles;
            break;
        }

        if (kDown & KEY_B)
        {
            status = AppStatus::SelectLocalFiles;
            break;
        }
        if (kDown & KEY_ZL)
        {
            status = AppStatus::ConfigScreen;
            break;
        }
        if (kDown & KEY_MINUS)
        {
            status = AppStatus::Exit;
            break;
        }
    }
}

void App::ConfigScreenView()
{
    Files files = Files();
    std::string url = "https://github.com/FabioBLK/SwitchSync";
    qrcodegen::QrCode qr0 = qrcodegen::QrCode::encodeText(&url[0u], qrcodegen::QrCode::Ecc::MEDIUM);
    std::string svg = qr0.toSvgString(4);

    std::string svgPath = "/switch/switchsync/svgInstructions.svg";
    files.saveFileData(svgPath, svg);
    SDL_Texture *image = uiManager.LoadTexture(&svgPath[0u]);

    KeyboardReader kb = KeyboardReader();

    SDL_Color selectedTextColor = {255, 0, 0, 255};
    SDL_Color regularTextColor = {255, 255, 255, 255};

    std::string appKeyLabel = "Application Key: ";
    std::string appSecretLabel = "Application Secret: ";

    std::string appKeyValue = (configData.appKey.empty() || configData.appKey == "NULL") ? "<EMPTY>" : configData.appKey;
    std::string appSecretValue = (configData.appSecret.empty() || configData.appSecret == "NULL") ? "<EMPTY>" : configData.appSecret;

    std::vector<std::string> optionsList;

    std::string warningString = "";
    optionsList.push_back(appKeyLabel + appKeyValue);
    optionsList.push_back(appSecretLabel + appSecretValue);

    int startBoxYPosition = 750;

    int position = 0;

    while (appletMainLoop())
    {
        position = tools::Clamp(position, 0, optionsList.size() - 1);

        uiManager.BeginFrame();
        uiManager.DrawConfigScreenView(url, bgColorWhiteTransparent, colorBlack, image, optionsList, position, startBoxYPosition);
        uiManager.DrawText(warningString, DISPLAY_WIDTH - 600, 25, colorBlack, 14);
        uiManager.EndFrame();

        hidScanInput();
        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if ((kDown & KEY_LSTICK_DOWN) || (kDown & KEY_DDOWN) || (kDown & KEY_L))
        {
            position++;
            buttonTime = 0;
        }
        else if ((kDown & KEY_LSTICK_UP) || (kDown & KEY_DUP) || (kDown & KEY_R))
        {
            position--;
            buttonTime = 0;
        }
        if (kDown & KEY_A)
        {
            kb.createKeyboard();
            kb.openKeyboard();

            if (position == 0)
            {
                appKeyValue = kb.getTypedText();
            }
            else
            {
                appSecretValue = kb.getTypedText();
            }
            optionsList.clear();
            optionsList.push_back(appKeyLabel + appKeyValue);
            optionsList.push_back(appSecretLabel + appSecretValue);
        }
        if (kDown & KEY_PLUS)
        {
            if (!appKeyValue.empty() && !appSecretValue.empty() && appKeyValue != "<EMPTY>" && appSecretValue != "<EMPTY>")
            {
                configData.appKey = appKeyValue;
                configData.appSecret = appSecretValue;

                std::string configFile = tools::getConfigString(configData);
                files.saveFileData(configFilePath, configFile);

                status = AppStatus::CheckStatus;
                break;
            }
            else
            {
                warningString = "You need to type a value in both AppKey and AppSecret";
            }
        }
        if (kDown & KEY_MINUS)
        {
            status = AppStatus::Exit;
            break;
        }
    }
}