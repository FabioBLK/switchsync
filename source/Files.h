#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <queue>

struct FilesData
{
    std::string name;
    bool isFolder;
    int type;
    int fileSize;
    long int mtime;
    bool selectedToTransfer;
};

class Files
{
private:
    
public:
    Files();
    ~Files();
    bool getDirData(std::string p_path, std::vector<FilesData> *p_data);
    bool getDirDataQueue(std::string p_path, std::queue<FilesData> *p_data);
    bool isDirValid(std::string p_path);
    bool createDir(std::string p_path);
    int getFileSize(std::string p_path);
    bool loadFileTXTData(std::string p_path, std::string *p_data);
    bool loadFileData(std::string p_path, FILE *p_fp, char *p_data, int p_fileSize);
    bool saveFileData(std::string p_path, std::string p_data);
    bool appendDataToFile(std::string p_path, std::string p_data);
    std::ofstream *openFileToSave(std::string p_path);
    FILE *openFileToDownload(std::string p_path);
    void closeFile(std::ofstream *p_id);
    static size_t writeCurlDataCallBack(char *ptr, size_t size, size_t nmemb, void *userdata);
    static size_t readCurlDataCallBack(char *buffer, size_t size, size_t nitems, void *instream);
};
