#include <stdlib.h>
#include <stdio.h>
#include "App.h"

int main(int argc, char *argv[])
{
    App app = App();
    app.StartApp();

    return 0;
}
