#include <string>
#include <vector>
#include <queue>

struct ConfigDataJSON
{
	std::string syncFolder;
	std::string dropboxAuth;
	std::string appKey;
	std::string appSecret;
	bool authorized = false;
	std::string accessToken;
};

struct AuthorizationTokenJSON
{
	std::string access_token;
	std::string token_type;
	std::string uid;
	std::string account_id;
};

struct FileInfoJSON 
{
	std::string tag;
	std::string name;
	std::string path_lower;
	std::string path_display;
	std::string id;
	std::string client_modified;
	std::string server_modified;
	std::string rev;
	int size;
	bool is_downloadable;
	std::string content_hash;
};

namespace tools
{
	int Clamp(int value, int min, int max);
	void LogToFile(std::string p_data);
	std::string newConfigObject();
	std::string getConfigString(ConfigDataJSON p_data);
	ConfigDataJSON parseConfigData(std::string p_data);
	AuthorizationTokenJSON parseAuthorizationTokenData(std::string p_data);
	std::queue<FileInfoJSON> parseFileInfoData(std::string p_data);
} // namespace tools