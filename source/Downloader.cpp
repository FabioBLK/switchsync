#include "Downloader.h"
#include "tools.h"
#include <fstream>
#include <switch.h>

Downloader::Downloader()
{
    socketInitializeDefault();
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curlHandle = curl_easy_init();
}

Downloader::~Downloader()
{
    socketExit();
    curl_global_cleanup();
}

int Downloader::DownloadFile(std::string p_url, std::string p_filePath)
{
    // Open File
    std::ofstream *fileID = files.openFileToSave(p_filePath);

    int httpResponse = 0;

    // Prepare Curl
    curlHandle = curl_easy_init();
    curl_easy_setopt(curlHandle, CURLOPT_URL, p_url.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, files.writeCurlDataCallBack);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &fileID);

    // Run Request
    curl_easy_perform(curlHandle);

    // Get response Info
    curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpResponse);

    // Close File
    files.closeFile(fileID);

    // Curl cleanup
    curl_easy_cleanup(curlHandle);

    return httpResponse;
}

int Downloader::DropboxDownloadFile(std::string p_userToken, std::string p_fileName, std::string p_saveFilePath)
{
    std::string tokenHeader = "Authorization: Bearer " + p_userToken;
    std::string fileNameHeader = "Dropbox-API-Arg: {\"path\": \"" + p_fileName + "\"}";

    // Prepare request Headers
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "cache-control: no-cache");
    headers = curl_slist_append(headers, "Connection: keep-alive");
    headers = curl_slist_append(headers, "Accept-Encoding: gzip, deflate");
    headers = curl_slist_append(headers, "Accept: */*");
    headers = curl_slist_append(headers, "User-Agent: PostmanRuntime/7.17.1");
    headers = curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, fileNameHeader.c_str());
    headers = curl_slist_append(headers, tokenHeader.c_str());

    std::string body = "";

    int response = NetworkDownload("GET", "https://content.dropboxapi.com/2/files/download", p_saveFilePath, headers, body);

    return response;
}

int Downloader::DropboxRequestToken(std::string p_authCode, std::string p_appId, std::string p_appSecret, std::string *p_outputStream)
{
    std::string postBody = "code=" + p_authCode + "&grant_type=authorization_code&client_id=" + p_appId + "&client_secret=" + p_appSecret;

    // Prepare request Headers
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "cache-control: no-cache");
    headers = curl_slist_append(headers, "Connection: keep-alive");
    headers = curl_slist_append(headers, "Accept-Encoding: gzip, deflate");
    headers = curl_slist_append(headers, "Accept: */*");
    headers = curl_slist_append(headers, "User-Agent: PostmanRuntime/7.17.1");
    headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded");

    int response = NetworkDownloadToMemory("POST", "https://api.dropboxapi.com/oauth2/token", p_outputStream, headers, postBody);

    return response;
}

int Downloader::DropboxRequestFileList(std::string p_userToken, std::string p_subfolderPath, std::string *p_outputStream)
{
    std::string tokenHeader = "Authorization: Bearer " + p_userToken;
    std::string postBody = "{\"path\": \"" + p_subfolderPath +"\",\"recursive\": false,\"include_media_info\": false,\"include_deleted\": false,\"include_has_explicit_shared_members\": false,\"include_mounted_folders\": true,\"include_non_downloadable_files\": true}";

    // Prepare request Headers
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "cache-control: no-cache");
    headers = curl_slist_append(headers, "Connection: keep-alive");
    headers = curl_slist_append(headers, "Accept-Encoding: gzip, deflate");
    headers = curl_slist_append(headers, "Accept: */*");
    headers = curl_slist_append(headers, "User-Agent: PostmanRuntime/7.17.1");
    headers = curl_slist_append(headers, "Content-Type: application/json; charset=utf-8");
    headers = curl_slist_append(headers, tokenHeader.c_str());

    int response = NetworkDownloadToMemory("POST", "https://api.dropboxapi.com/2/files/list_folder", p_outputStream, headers, postBody);

    return response;
}

int Downloader::DropboxUploadFile(std::string p_userToken, std::string p_fileName, std::string p_uploadFilePath)
{
    std::string tokenHeader = "Authorization: Bearer " + p_userToken;
    std::string fileNameHeader = "Dropbox-API-Arg: {\"path\": \"" + p_fileName + "\",\"mode\": \"overwrite\",\"autorename\": false,\"mute\": true,\"strict_conflict\": false}";

    // Prepare request Headers
    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "cache-control: no-cache");
    headers = curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Connection: keep-alive");
    headers = curl_slist_append(headers, "Accept-Encoding: gzip, deflate");
    headers = curl_slist_append(headers, "Accept: */*");
    headers = curl_slist_append(headers, tokenHeader.c_str());
    headers = curl_slist_append(headers, fileNameHeader.c_str());

    int response = NetworkUpload("POST", "https://content.dropboxapi.com/2/files/upload", p_uploadFilePath, headers);

    return response;
}

int Downloader::DropboxCreateFolder(std::string p_userToken, std::string p_fileName, std::string *p_outputStream)
{
    std::string tokenHeader = "Authorization: Bearer " + p_userToken;
    std::string postBody = "{\r\n    \"path\": \"/" + p_fileName + "\",\r\n    \"autorename\": false\r\n}";

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "cache-control: no-cache");
    headers = curl_slist_append(headers, "Connection: keep-alive");
    headers = curl_slist_append(headers, "Accept-Encoding: gzip, deflate");
    headers = curl_slist_append(headers, "Content-Length: 236");
    headers = curl_slist_append(headers, "Accept: */*");
    headers = curl_slist_append(headers, "User-Agent: PostmanRuntime/7.17.1");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, tokenHeader.c_str());

    int response = NetworkDownloadToMemory("POST", "https://api.dropboxapi.com/oauth2/files/create_folder_v2", p_outputStream, headers, postBody);

    return response;
}

int Downloader::NetworkDownload(std::string p_httpVerb, std::string p_apiUrl, std::string p_filePath, curl_slist *p_headers, std::string p_body)
{
    // Open File
    std::ofstream fileID;
    fileID.open(p_filePath, std::ofstream::out | std::ofstream::trunc);

    int httpResponse = 0;

    // Prepare Curl
    curlHandle = curl_easy_init();
    curl_easy_setopt(curlHandle, CURLOPT_CUSTOMREQUEST, p_httpVerb.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_URL, p_apiUrl.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, false);

    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, files.writeCurlDataCallBack);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &fileID);

    curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, p_headers);

    if (p_httpVerb == "POST")
    {
        curl_easy_setopt(curlHandle, CURLOPT_ACCEPT_ENCODING, "gzip");
        curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, &p_body[0u]);
        curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, (long)p_body.size());
    }

    // Run Request
    curl_easy_perform(curlHandle);

    // Get response Info
    curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpResponse);

    // Close File
    fileID.close();

    // Curl cleanup
    curl_easy_cleanup(curlHandle);

    return httpResponse;
}

int Downloader::NetworkDownloadToMemory(std::string p_httpVerb, std::string p_apiUrl, std::string *p_outputStream, curl_slist *p_headers, std::string p_body)
{
    int httpResponse = 0;

    // Prepare Curl
    curlHandle = curl_easy_init();
    curl_easy_setopt(curlHandle, CURLOPT_CUSTOMREQUEST, p_httpVerb.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_URL, p_apiUrl.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, false);

    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, writeCurlToStringCallBack);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, p_outputStream);

    curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, p_headers);

    if (p_httpVerb == "POST")
    {
        curl_easy_setopt(curlHandle, CURLOPT_ACCEPT_ENCODING, "gzip, deflate");
        curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, &p_body[0u]);
        curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, (long)p_body.size());
    }

    // Run Request
    curl_easy_perform(curlHandle);

    // Get response Info
    curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpResponse);

    // Curl cleanup
    curl_easy_cleanup(curlHandle);

    return httpResponse;
}

int Downloader::NetworkUpload(std::string p_httpVerb, std::string p_apiUrl, std::string p_uploadFilePath, curl_slist *p_headers)
{
    int httpResponse = 0;
    int fileSize = files.getFileSize(p_uploadFilePath);
    char *filebuf = new char[fileSize];
    FILE *fp = NULL;
    bool fileLoad = files.loadFileData(p_uploadFilePath, fp, filebuf, fileSize);
    if (!fileLoad)
    {
        return 0;
    }

    // Prepare Curl
    curlHandle = curl_easy_init();
    curl_easy_setopt(curlHandle, CURLOPT_CUSTOMREQUEST, p_httpVerb.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_URL, p_apiUrl.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curlHandle, CURLOPT_ACCEPT_ENCODING, "gzip");
    curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, filebuf);
    curl_easy_setopt(curlHandle, CURLOPT_READDATA, (void *)fp);
    curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE_LARGE, (curl_off_t)fileSize);

    curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, p_headers);

    // Run Request
    curl_easy_perform(curlHandle);

    // Get response Info
    curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpResponse);

    delete filebuf;
    // Close File
    if (fp)
    {
        fclose(fp);
    }

    // Curl cleanup
    curl_easy_cleanup(curlHandle);

    return httpResponse;
}

size_t Downloader::writeCurlToStringCallBack(void *contents, size_t size, size_t nmemb, std::string *s)
{
    size_t newLength = size * nmemb;
    try
    {
        s->append((char *)contents, newLength);
    }
    catch (std::bad_alloc &e)
    {
        //handle memory problem
        return 0;
    }
    return newLength;
}